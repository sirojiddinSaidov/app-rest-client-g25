package uz.pdp.apprestclient.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import uz.pdp.apprestclient.payload.DistrictDTO;
import uz.pdp.apprestclient.payload.RegionDTO;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final RestTemplate restTemplate;

    @GetMapping("/region")
    public HttpEntity<List<RegionDTO>> regions() {
        return getListResponseEntity();
    }

    @GetMapping("/district/{regionId}")
    public HttpEntity<List<DistrictDTO>> districtsByRegionId(@PathVariable String regionId) {

        DistrictDTO districtDTO = DistrictDTO.builder()
                .regionId(regionId)
                .name("Ketmon")
                .build();
        DistrictDTO block = WebClient.create()
                .post()
                .uri("http://localhost:8080/district")
                .body(Mono.just(districtDTO), DistrictDTO.class)
                .retrieve()
                .bodyToMono(DistrictDTO.class)
                .block();

        System.out.println(block);


        List<DistrictDTO> districtDTOList = WebClient.create()
                .get()
                .uri("http://localhost:8080/district/by-region/" + regionId)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<DistrictDTO>>() {
                })
                .block();

        return ResponseEntity.ok(districtDTOList);
    }



    private void exchangePost(RestTemplate restTemplate) {
        RegionDTO regionDTO = RegionDTO.builder().name("Surxondaryo")
                .build();
        HttpEntity<RegionDTO> httpEntity = new HttpEntity<>(regionDTO);
        ResponseEntity<RegionDTO> exchange = restTemplate.exchange("http://localhost:8080/region", HttpMethod.POST, httpEntity, RegionDTO.class);
        System.out.println(exchange.getStatusCode());
        System.out.println(exchange.hasBody());
    }

    private void postForEntity(RestTemplate restTemplate, RegionDTO regionDTO) {
        ResponseEntity<RegionDTO> regionDTOResponseEntity = restTemplate.postForEntity("http://localhost:8080/region", regionDTO, RegionDTO.class);
        System.out.println(regionDTOResponseEntity.getStatusCode());
        System.out.println(regionDTOResponseEntity.getBody());
    }

    private ResponseEntity<List<RegionDTO>> getListResponseEntity() {
        ResponseEntity<List<RegionDTO>> listResponseEntity = restTemplate.exchange("http://localhost:8080/region",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<RegionDTO>>() {
                });

        System.out.println(listResponseEntity.getStatusCode());
        System.out.println(listResponseEntity.getBody());
        return listResponseEntity;
    }

    private void mapToArrayGetForEntity(RestTemplate restTemplate) {
//        ResponseEntity<RegionDTO[]> forEntity = restTemplate.getForEntity(URI.create("http://localhost:8080/region"),
        ResponseEntity<RegionDTO[]> forEntity = restTemplate.getForEntity("http://localhost:8080/region",
                RegionDTO[].class);

        System.out.println(forEntity.getStatusCode());
        System.out.println(Arrays.toString(forEntity.getBody()));
    }

}
