package uz.pdp.apprestclient.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import uz.pdp.apprestclient.feign.RegionOpenFeign;
import uz.pdp.apprestclient.payload.DistrictDTO;
import uz.pdp.apprestclient.payload.RegionDTO;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {
    private final RegionOpenFeign regionOpenFeign;

    @GetMapping("/region")
    public HttpEntity<List<RegionDTO>> regions() {
        List<RegionDTO> regions = regionOpenFeign.getRegions();
        return ResponseEntity.ok(regions);
    }

    @GetMapping("/region-add")
    public HttpEntity<RegionDTO> add(@RequestParam String name) {
        RegionDTO regionDTO = RegionDTO.builder()
                .name(name)
                .build();

        RegionDTO savedRegion = regionOpenFeign.addRegion(regionDTO);
        return ResponseEntity.ok(savedRegion);
    }


}
