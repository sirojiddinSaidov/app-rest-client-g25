package uz.pdp.apprestclient.feign;


import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import uz.pdp.apprestclient.payload.RegionDTO;

import java.util.List;

@FeignClient(name = "bla", url = "http://localhost:8080")
public interface RegionOpenFeign {

    @GetMapping("/region")
    List<RegionDTO> getRegions();

    @PostMapping("/region")
    RegionDTO addRegion(@RequestBody RegionDTO regionDTO);
}
